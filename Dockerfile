#TO okdhryk/siurus17-dev:noetic
FROM osrf/ros:noetic-desktop-full
LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"

WORKDIR /root/
RUN  apt-get update &&  apt-get install -q -y --no-install-recommends \
     vim wget curl less sudo git curl python3-catkin-lint python3-pip x11-apps \
    && rm -rf /var/lib/apt/lists/*
RUN sudo ln -s /usr/bin/python3 /usr/bin/python


ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV ROS_DISTRO noetic
ARG USERNAME=roboworks
ARG GROUPNAME=roboworks
ARG UID=1000
ARG GID=1000
ARG PASSWORD=tamagawa
RUN groupadd -g $GID $GROUPNAME && \
    useradd -m -s /bin/bash -u $UID -g $GID -G sudo $USERNAME && \
    echo $USERNAME:$PASSWORD | chpasswd && \
    echo "$USERNAME   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER $USERNAME
WORKDIR /home/$USERNAME/

# nvidia-container-runtime（描画するための環境変数の設定）
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

RUN mkdir -p /home/$USERNAME/catkin_ros/src && \
    cd /home/$USERNAME/catkin_ros/src && \
    /bin/bash -c "source /opt/ros/noetic/setup.bash; catkin_init_workspace" && \
#    git clone https://github.com/rt-net/sciurus17_ros.git && \
    cd /home/$USERNAME/catkin_ros && \
    sudo apt-get update && \
    rosdep update && \
    rosdep install --from-paths src --ignore-src -r -y && \
    /bin/bash -c "source /opt/ros/noetic/setup.bash; catkin_make"  && \
    sudo  apt-get clean

RUN cd ~/ && \
    git config --global user.email "you@example.com" && \
    git config --global user.name "Your Name" && \
    git clone https://github.com/wmcnally/kapao.git  && \
    cd ~/kapao  && \
    pip install gdown  && \
    python data/scripts/download_models.py
COPY requirements_trcp.txt /home/roboworks/kapao/
RUN cd ~/kapao && pip install -r requirements_trcp.txt
