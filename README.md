# KapaoをDockerで動かす

## Kapao（Keypoints and Poses as Objects）
Kapaoはヒートマップを使わない姿勢推定のアルゴリズムです。

従来のヒートマップを使った回帰のアプローチが大量の計算資源を有し、GPUでの動作が基本となります。

一方、Kapaoはヒートマップを使わないのでCPUでも実用的な速度が期待できます。

[オリジナルの文献はこちら　](http://cedro3.com/ai/kapao/#:~:text=%EF%BC%8A%E3%81%93%E3%81%AE-,%E8%AB%96%E6%96%87,-%E3%81%AF%E3%80%812021.11%E3%81%AB)

オリジナルのGithub

https://github.com/wmcnally/kapao

　＃＃ インストール
お使いの環境にDockerがインストールされているのを確認してください。

下記はUbuntu２０.04にインストールする場合を例に説明します。

```
$ cd ~/
$ git clone https://gitlab.com/okadalaboratory/myprojects/kapao-docker.git
$ cd kapao-docker

$ docker-compose build
```
Dockerfileのビルドでエラーが出なければ、下記のようにコンテナを生成してください。
```
$ cd ~/kapao-docker/
$ docker-compose up
```

コンテナの生成でエラーが出なければ、別のターミナルを開き、下記のようにコンテナに入ってください。
```
$ docker exec -it kapao-dev bash
```

## デモプログラムの実行
公式サイトに予め用意されているデモプログラムは下記のように実行します。

### 静止画
```
$ cd ~/kapao
$ python demos/image.py --bbox
$ python demos/image.py --bbox --pose --face --no-kp-dets
$ python demos/image.py --bbox --pose --face --no-kp-dets --kp-bbox
$ python demos/image.py --pose --face
```

### 動画
```
$ python demos/video.py --face --display
```
表示された動画を保存するには下記のように実行します。
```
$ cd ~/kapao
$ python demos/video.py --face --device cpu --gif
```

その他のデモに関しては [kapaoの公式Github](https://github.com/wmcnally/kapao)をご覧ください。

# パフォーマンス
[公式サイトより](https://github.com/wmcnally/kapao/raw/master/res/accuracy_latency.png)

動画のでもプログラムで測定
## 公式サイト
9.5FPS<br>
Intel Core i7-8700K<br>
16GB DDR4 3000MHz<br>
Samsung 970 Pro M.2 NVMe SSD<br>

## ThinkPad X1 Extreme 2nd
9.1FPS(Docker)<br>
???FPS(host)<br>
Intel Core i9-9880H 2.30GHz<br>
64GB DDR4 3000MHz<br>
Ubuntu 20.04

## ASUS ROG Flow Z13 (2022)
Intel Core i9-12900H<br>
16GB<br>
GeForce RTX™ 3050 Ti<br>

***Ubuntu 20.04<br>
思ったより遅い
3.3FPS(Docker)<br>
3.2FPS(host) <br>

***Windows11 WSL2 + Ubuntu 20.04<br>
とても早い！
9.0 FPS(Docker)<br>
xxx FPS(host) <br>



#　これからやること
ROS対応

GPUを使って動かす

Webカメラからの入力(12/30 済)

# memo
```
docker run -it --privileged --net=host --ipc=host -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY -v $HOME/.Xauthority:/home/$(id -un)/.Xauthority -e XAUTHORITY=/home/$(id -un)/.Xauthority okdhryk/kapao-dev:noetic bash
```
macOS
```
docker exec -e DISPLAY=host.docker.internal:0 -it kapao-dev bash
```

Webカメラ対応

https://mirai-tec.hatenablog.com/?page=1659876849
